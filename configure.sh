#!/bin/bash

export SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo $SCRIPTDIR

configure_non_work_tools() {
  chmod +x ./other/configure.sh
  $SCRIPTDIR/other/configure.sh
}

install_atom() {
  OS=$1
  echo "Installing Atom."

  echo "Running config script."
  chmod +x ./atom/configure_atom.sh
  $SCRIPTDIR/atom/configure_atom.sh $OS
}

install_mullvad() {
  echo "Installing Mullvad"

  if [[ $OS == "ubuntu" ]]; then
    wget --content-disposition https://github.com/mullvad/mullvadvpn-app/releases/download/2022.1/MullvadVPN-2022.1_amd64.deb
    sudo apt install ./MullvadVPN-*
  elif [[ $OS == "fedora" ]]; then
    wget --content-disposition https://mullvad.net/download/latest-rpm-app
    sudo dnf -y install ./MullvadVPN-*
  fi


}

install_zsh() {
  OS=$1

  echo "Installing zsh"
  chmod +x ./zsh/$OS/install.sh
  $SCRIPTDIR/zsh/$OS/install.sh

  echo "Setting zsh as default shell."
  chsh -s /bin/zsh

  echo "Shell changed successfully.  Change will not take effect until terminal is restarted."
}


# Detect distro and set variable
# Checking different options in order of likelihood
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$(echo "$NAME" | tr '[:upper:]' '[:lower:]')
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    echo "not implemented"
elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    echo "not implemented"
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

echo "Updating packages"

if [[ $OS == "ubuntu" ]]; then
  sudo apt-get update && sudo apt-get upgrade
  sudo apt-get install gnome-tweak-tool
elif [[ $OS == "fedora" ]]; then
  sudo dnf update
  sudo dnf install gnome-tweak-tool
  sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
fi

read -p "Install and configure Mullvad?" vpn
case $vpn in
  [Yy]* ) install_mullvad $vpn;;
  [Nn]* ) echo "Not installing Mullvad";;
esac

read -p "Install and configure Atom?" atom
case $atom in
  [Yy]* ) install_atom $OS;;
  [Nn]* ) echo "Not installing Atom.";;
esac

read -p "Install and configure zsh?" zsh
case $zsh in
  [Yy]* ) install_zsh $OS;;
  [Nn]* ) echo "Not installing zsh";;
esac

configure_vim

configure_non_work_tools

echo "Configuration complete!"
